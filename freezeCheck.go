package main

/* This program should: Notify me when it may freeze tonight (<40F)

Goals:
1. (✅) Get min temp for today (and write log line)
2. () Retry logic if API call fails (and write log line)
3. () Notify me if multiple API failures (and write log line)
4. (✅) Notify me if tonight's min is less than 40F (and write log line)

*/

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gregdel/pushover"
)

var logFile *os.File
var outToTerminal, reminders bool

type Forecast struct {
	Daily struct {
		Time             []string  `json:"time"`
		Temperature2mMin []float64 `json:"temperature_2m_min"`
	} `json:"daily"`
}

func main() {
	var err error

	processCommandLineArgs()

	// Open log file
	logFile, err = os.OpenFile("freezeCheck.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		sendNotification("Could not open log file.")
		fmt.Println(err)
		os.Exit(1)
	}
	defer logFile.Close()

	// Log application startup
	writeLogLine("FreezeCheck Application startup")

	latitude := os.Getenv("FREEZECHECK_LATITUDE")
	longitude := os.Getenv("FREEZECHECK_LONGITUDE")
	timezone := os.Getenv("FREEZECHECK_TIMEZONE")

	url := fmt.Sprint("https://api.open-meteo.com/v1/forecast?latitude=", latitude, "&longitude=", longitude, "&daily=temperature_2m_min&temperature_unit=fahrenheit&timezone=", timezone, "&forecast_days=1")

	resp, err := http.Get(url)
	if err != nil || resp.StatusCode >= 400 {
		var errorLogDetail string

		// Send generic error notification ahead of logic below to build log line
		sendNotification("Error in open-meteo API call. Check logs.")

		// Oh look, the first thing I've found that I don't like about Go.. no ternary operator? Boo!
		if err != nil {
			errorLogDetail = err.Error()
		} else {
			errorLogDetail = resp.Status
		}

		writeLogLine("Error in API call: " + errorLogDetail)
		os.Exit(2)
	}

	defer resp.Body.Close()

	var forecast Forecast
	if err := json.NewDecoder(resp.Body).Decode(&forecast); err != nil {
		sendNotification("Error decoding open-meteo API response. Check logs.")
		writeLogLine("Error decoding API response: " + err.Error())
		os.Exit(3)
	}

	minTempToday := forecast.Daily.Temperature2mMin[0]

	if minTempToday < 32 {
		sendNotification(fmt.Sprintf("It's going to freeze tonight! (min temp = %.2f)", minTempToday))
	} else if minTempToday < 40 {
		sendNotification(fmt.Sprintf("It might freeze tonight! (min temp = %.2f)", minTempToday))
	} else {
		writeLogLine(fmt.Sprintf("No temperature notification sent. Tonight's low is %.2f", minTempToday))
		if reminders && time.Now().Weekday() == 6 {
			sendNotification("I'm still running! (even if you haven't heard from me in a while)")
		}
	}
}

func sendNotification(message string) {
	pushoverApiToken := os.Getenv("PUSHOVER_API_TOKEN")
	pushoverUserToken := os.Getenv("PUSHOVER_USER_TOKEN")

	// Create a new pushover app with a token
	app := pushover.New(pushoverApiToken)

	// Create a new recipient
	recipient := pushover.NewRecipient(pushoverUserToken)

	// Create the message to send
	pushOverMessage := pushover.NewMessage(message)

	// Send the message to the recipient
	_, err := app.SendMessage(pushOverMessage, recipient)
	if err != nil {
		writeLogLine("Error in sendNotification: " + err.Error())
		os.Exit(4)
	} else {
		writeLogLine("Notification sent! \"" + message)
	}

}

func writeLogLine(logLine string) {

	_, err := logFile.WriteString(time.Now().Format("2006-01-02 15:04:05") + " " + string(logLine) + "\n")
	if err != nil {
		fmt.Println(time.Now().Format("2006-01-02 15:04:05") + " " + err.Error())
		return
	}

	if outToTerminal {
		println(time.Now().Format("2006-01-02 15:04:05") + " " + logLine)
	}
}

func processCommandLineArgs() {
	if len(os.Args) > 1 {
		for _, arg := range os.Args {
			switch arg {
			case "echo":
				outToTerminal = true
			case "reminders":
				reminders = true
			}
		}
	}
}
